#!/usr/bin/env python3

# qsplit: Copyright (C) 2020 John C. Bowman

import lxml.etree as ET
from datetime import datetime
from slugify import slugify
import argparse

P=argparse.ArgumentParser(description='Split Moodle Stack library into individual XML questions.')
required=P.add_argument_group('required arguments')
required.add_argument('-u',metavar='ccid',help='Author ID',required=True)
P.add_argument('library',help='Question library')
args=P.parse_args()

CCID=args.u
bank=args.library

parser=ET.XMLParser(strip_cdata=False)

start=int(datetime.now().strftime('%s'))
tree=ET.parse(bank,parser=parser)
root=tree.getroot()

category=None
categories=[]

for child in root:
    if 'type' in child.keys():
        if child.attrib['type'] == 'category':
            category=child
            categories=child.find('category').find('text').text.split('/')[2:]
            print(categories)
        else:
            name=child.find('name').find('text')
            id=child.find('idnumber')
            if id.text == None:
                id.text=CCID+'-'+str(datetime.fromtimestamp(start).strftime('%F-%H-%M'))
                ccid=CCID
            else:
                ccid=id.text.split('-')[0]

            suffix=' id='+id.text
            start += 60
            tags=child.find('tags')
            if tags != None:
                Tags=[]
                for tag in tags:
                    Tags.append(tag.find('text').text)
                for cat in categories:
                    if cat not in Tags:
                        tag=ET.Element('tag')
                        c=ET.SubElement(tag,'text')
                        c.text=cat
                        tags.append(tag)

            filename=slugify(name.text+'-'+ccid)+'.xml'
            top=ET.Element('quiz')
            question=ET.ElementTree(top)
            qroot=question.getroot()
            if category != None:
                qroot.append(category)

            qroot.append(child)
            question.write(filename)
            open(filename,'a').write('\n')
            print(filename+suffix)
