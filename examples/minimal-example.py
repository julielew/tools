question={
  "questiontext": """
If $f(x)=@f@$, then $f'(x)=$ [[input:derivative]].
""",

  "questionvariables": """
f:2^x;
_derivative:diff(f,x);
""",
}
