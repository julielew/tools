#!/usr/bin/env python3

# qjoin: Copyright (C) 2020 John C. Bowman

import lxml.etree as ET
import argparse

P=argparse.ArgumentParser(description='Join Moodle Stack XML questions into library.')
required=P.add_argument_group('required arguments')
required.add_argument('-o',metavar='library',help='Output library',required=True)
P.add_argument('question',nargs='+',help='Input question')
args=P.parse_args()

parser=ET.XMLParser(strip_cdata=False)

top=ET.Element('quiz')
tree=ET.ElementTree(top)
root=tree.getroot()
out=args.o

for file in args.question:
    question=ET.parse(file,parser=parser)
    for child in question.getroot():
        root.append(child)

tree.write(out)
open(out,'a').write('\n')
